import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TouringPageRoutingModule } from './touring-routing.module';

import { TouringPage } from './touring.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TouringPageRoutingModule
  ],
  declarations: [TouringPage]
})
export class TouringPageModule {}
