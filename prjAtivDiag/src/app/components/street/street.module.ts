import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StreetPageRoutingModule } from './street-routing.module';

import { StreetPage } from './street.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StreetPageRoutingModule
  ],
  declarations: [StreetPage]
})
export class StreetPageModule {}
