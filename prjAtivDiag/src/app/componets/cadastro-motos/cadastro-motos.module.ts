import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroMotosPageRoutingModule } from './cadastro-motos-routing.module';

import { CadastroMotosPage } from './cadastro-motos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroMotosPageRoutingModule
  ],
  declarations: [CadastroMotosPage]
})
export class CadastroMotosPageModule {}
