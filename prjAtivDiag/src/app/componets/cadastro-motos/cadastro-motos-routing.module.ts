import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroMotosPage } from './cadastro-motos.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroMotosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroMotosPageRoutingModule {}
