import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'street',
    loadChildren: () => import('./components/street/street.module').then( m => m.StreetPageModule)
  },
  {
    path: 'trail',
    loadChildren: () => import('./components/trail/trail.module').then( m => m.TrailPageModule)
  },
  {
    path: 'scooter',
    loadChildren: () => import('./components/scooter/scooter.module').then( m => m.ScooterPageModule)
  },
  {
    path: 'sport',
    loadChildren: () => import('./components/sport/sport.module').then( m => m.SportPageModule)
  },
  {
    path: 'touring',
    loadChildren: () => import('./components/touring/touring.module').then( m => m.TouringPageModule)
  },
  {
    path: 'cadastro-usuarios',
    loadChildren: () => import('./componets/cadastro-usuarios/cadastro-usuarios.module').then( m => m.CadastroUsuariosPageModule)
  },
  {
    path: 'cadastro-clientes',
    loadChildren: () => import('./componets/cadastro-clientes/cadastro-clientes.module').then( m => m.CadastroClientesPageModule)
  },
  {
    path: 'cadastro-motos',
    loadChildren: () => import('./componets/cadastro-motos/cadastro-motos.module').then( m => m.CadastroMotosPageModule)
  },
  {
    path: 'acompanhamento-pedidos',
    loadChildren: () => import('./componets/acompanhamento-pedidos/acompanhamento-pedidos.module').then( m => m.AcompanhamentoPedidosPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
